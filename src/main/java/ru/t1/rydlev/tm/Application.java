package ru.t1.rydlev.tm;

import static ru.t1.rydlev.tm.constant.TerminalConst.*;

public class Application {

    public static void main(String[] args) {
        parseArguments(args);
    }

    private static void parseArguments(final String[] args) {
        if (args == null || args.length == 0) showWelcome();
        final String arg = args[0];
        parseArgument(arg);
    }

    private static void parseArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case HELP:
                showHelp();
                break;
            case VERSION:
                showVersion();
                break;
            case INFO:
                showDeveloperInfo();
                break;
            default:
                showWelcome();
        }
    }

    private static void showWelcome() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.exit(0);
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show application commands. \n", HELP);
        System.out.printf("%s - Show application version. \n", VERSION);
        System.out.printf("%s - Show developer info. \n", INFO);
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.3.0");
    }

    private static void showDeveloperInfo() {
        System.out.println("[DEVELOPER]");
        System.out.println("NAME: Oleg Rydlev");
        System.out.println("E-MAIL: work.akrr@gmail.com");
    }

}